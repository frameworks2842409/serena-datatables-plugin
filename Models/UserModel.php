<?php

namespace Plugins\Datatables\Models;

use Plugins\Datatables\Traits\ModelDT;
use Serena\Models\UserModel as BaseModel;
use Serena\Traits\Singleton;

class UserModel extends BaseModel
{
  use Singleton, ModelDT;
}