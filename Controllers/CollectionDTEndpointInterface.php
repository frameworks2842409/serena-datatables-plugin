<?php

namespace Plugins\Datatables\Controllers;

interface CollectionDTEndpointInterface
{
  public function collectiondt(): \Serena\Utils\ViewInterface;

  public function collectiondtDoc(): \Serena\Utils\ApiDoc;

  public function getCollectionView(): \Serena\Utils\ViewInterface;

  public function configModel(): void;

  public function fetch(): \Serena\Utils\ViewInterface;
}