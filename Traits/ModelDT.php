<?php

namespace Plugins\Datatables\Traits;

trait ModelDT
{
  private bool $isDatatable = TRUE;

  public function getOrder(): ?string
  {
    if (isset($_GET['order']) && is_array($_GET['order'])) {
      $order = [];
      foreach ($_GET['order'] as $ord) {
        $order[] = $_GET['columns'][$ord['column']]['data'] . ' ' . strtoupper($ord['dir']);
      }
      return implode(', ', $order);
    }

    return NULL;
  }

  public function setDatatable($isDatatable): self
  {
    $this->isDatatable = $isDatatable;

    return $this;
  }

  public function filter(): array
  {
    $filters = [];
    $search = $_GET['search']['value'] ?? FALSE;

    if ($search) {
      $filters[] = new \Serena\Models\Filter('id LIKE :id', $search);
    }

    return $filters;
  }
}