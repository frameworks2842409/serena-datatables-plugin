#!/usr/bin/env php
<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../autoload.php';
@include __DIR__ . '/../../app/vendor/autoload.php';

define('ROOT', dirname(__DIR__, 2));
define('CACHE_PATH', ROOT . '/cache');

use Serena\Managers\UtilsManager;
use Serena\Utils\Console;

try {
  UtilsManager::checkConfig();
} catch (\Serena\Exceptions\SystemException $e) {
  die("Error checking config");
}

$console = new Console($argv);

exit($console->setProfile(basename(__DIR__))->execute());