<?php

namespace Plugins\Datatables\Traits;

trait CollectionDTController
{
  public function configModel(): void
  {
    if (!($this->getModel() instanceof \Serena\Models\ModelInterface)) {
      throw new \Serena\Exceptions\SystemException(__CLASS__ . ' must set a ModelInterface class on $this->model');
    }

    $this->getModel()->setStart();
    $this->getModel()->setLimit();
  }

  public function fetch(): \Serena\Utils\ViewInterface
  {
    $data = $this->getModel()->load();
    $view = \Plugins\Datatables\Utils\ViewDataTables::getInstance();
    $view->setRecordsTotal($this->getModel()->getCountTotal());
    $view->setRecordsFiltered($this->getModel()->getCountFiltered());
    $view->setData($data);

    return $view;
  }
}