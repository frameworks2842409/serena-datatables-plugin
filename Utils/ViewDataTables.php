<?php

namespace Plugins\Datatables\Utils;

use Serena\Traits\Singleton;
use Serena\Utils\AbstractView;
use Serena\Utils\ViewInterface;

class ViewDataTables extends AbstractView implements ViewInterface
{
  use Singleton;

  private array $data;
  private int $recordsTotal;
  private int $recordsFiltered;

  public function setData($data)
  {
    $this->data = $data;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getRecordsTotal()
  {
    return $this->recordsTotal;
  }

  /**
   * @param mixed $recordsTotal
   * @return ViewDataTables
   */
  public function setRecordsTotal($recordsTotal)
  {
    $this->recordsTotal = $recordsTotal;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getRecordsFiltered()
  {
    return $this->recordsFiltered;
  }

  /**
   * @param mixed $recordsFiltered
   * @return ViewDataTables
   */
  public function setRecordsFiltered($recordsFiltered)
  {
    $this->recordsFiltered = $recordsFiltered;
    return $this;
  }



  public function getView()
  {
    return [
      'recordsTotal' => $this->getRecordsTotal(),
      'recordsFiltered' => $this->getRecordsFiltered(),
      'data' => $this->data
    ];
  }

  public static function __set_state(array $properties): ViewInterface
  {
    $view = self::getInstance();

    return self::__set_vars($view, $properties);
  }
}